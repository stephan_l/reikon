#' reikon
#'
#' A package to retrieve data from Thomson Reuters Eikon platform
#'
#' @docType package
#' @name reikon
#' @import rClr
NULL
