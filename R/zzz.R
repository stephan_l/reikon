eikon_class_name <- "reikon.Eikon"
interval_enums <- NULL

.onLoad <- function(libname, pkgname) {
  p <- if (.Machine$sizeof.pointer == 8) "x64" else "i386"
  rClr::clrLoadAssembly(system.file(paste0("NetDlls/", p, "/reikon.dll"), package = "reikon"))
  rClr::clrCallStatic(eikon_class_name, "Init")
  interval_enums <<- clrGetEnumNames("ThomsonReuters.Desktop.SDK.DataAccess.TimeSeries.CommonInterval")
}

.onUnload <- function(libpath) {
  rClr::clrCallStatic(eikon_class_name, "Terminate")
  rClr::clrShutdown()
}

