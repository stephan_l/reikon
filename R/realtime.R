#' Return realtime data
#'
#' Returns realtime data from Eikon. Currently, it supports only numeric fields
#'
#' The first time the function is called for a combination of ric and field, a subscription is created.
#' Therefore, the first call might take longer. Further calls for the same ric and field should be
#' significately faster. The function works by polling an internal .NET dictionary that keeps the latest
#' values for each ric and field combination
#'
#' @param ric The RIC of the requested data
#' @param field The field of the requested data
#' @param poll_freq How much to wait in seconds between each poll
#' @param poll_max_times How many times to poll before returning if no data is received
#'
#' @export
#'
#' @examples
#' get_data("EUR=", "ASK")
#' get_data("ARRO153=BA", "BID")
#'
#' @seealso subscribe
get_data <- function(ric, field, poll_freq = 0.1, poll_max_times = 10) {
  r <- function() rClr::clrCallStatic(eikon_class_name, "GetValue", ric = ric, field = field)
  s <- function() rClr::clrCallStatic(eikon_class_name, "Subscribe", ric = ric, field = field)
  x <- request_or_subscribe_and_wait(r = r, s = s, freq = poll_freq, max_times = poll_max_times)
  x
}

#' Subscribes to realtime data from Eikon
#'
#' Subscribing doesn't return anything, it just initiates the subscription and allows getting the data
#' faster afterwards.
#'
#' It isn't necessary to call this function previous to calling \code{\link{get_data}}.
#' If you know beforehand which data you 'll need, you can subscribe during your code setup
#' (or package initialization) and save some time afterwards
#'
#' @param ric The RIC of the requested data
#' @param field The field of the requested data
#'
#' @export
#'
#' @examples
#' subscribe("EUR=", "ASK")
#' subscribe("ARRO153=BA", "BID")
#'
#' @seealso subscribe
subscribe <- function(ric, field) {
  rClr::clrCallStatic(eikon_class_name, "Subscribe", ric = ric, field = field)
}

get_zero <- function(x) {
  rClr::clrCallStatic(eikon_class_name, "GetZero")
}
