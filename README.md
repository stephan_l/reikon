`reikon` is a package that allows the user to retrieve data from the Thomson Reuters Eikon platform.

The usage is very simple
```
library(reikon)
get_data("EUR=", "ASK")
get_history("EUR=")
```

You should be able to install it using `devtools::install_bitbucket("juancentro/reikon")`.
Please note that `reikon` depends on `rClr`, which you can find [here](https://github.com/jmp75/rClr).

For a technical description on how the package works, check the vignette.
